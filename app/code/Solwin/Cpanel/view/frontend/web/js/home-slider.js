require(['jquery', 'slick'], function($){
    $(function(){
               $(".parent-slick").slick({
                infinite: false,
                dots: true,
                arrows: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1
            });
    });

         $('.ask-quote-btn').click(function(){
        $('.quote-content .quote').toggleClass('show');
        $('.ask-quote-btn').toggleClass('grey-color');
    });
});