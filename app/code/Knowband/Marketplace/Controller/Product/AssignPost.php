<?php

namespace Knowband\Marketplace\Controller\Product;

use Knowband\Marketplace\Controller\Index\ParentController;
use Magento\Framework\Controller\ResultFactory;
use Magento\Downloadable\Api\Data\SampleInterfaceFactory as SampleFactory;
use Magento\Downloadable\Api\Data\LinkInterfaceFactory as LinkFactory;
class AssignPost extends ParentController {

    protected $mp_resultRawFactory;
    protected $mp_request;
    protected $mp_scopeConfig;
    protected $inlineTranslation;
    protected $mp_transportBuilder;
    
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var SampleFactory
     */
    private $sampleFactory;

    /**
     * @var LinkFactory
     */
    private $linkFactory;

    /**
     * @var \Magento\Downloadable\Model\Sample\Builder
     */
    private $sampleBuilder;

    /**
     * @var \Magento\Downloadable\Model\Link\Builder
     */
    private $linkBuilder;

    public function __construct(
            \Magento\Framework\App\Action\Context $context, 
            \Magento\Framework\App\Request\Http $request, 
            \Magento\Framework\App\Response\Http $response,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Framework\Registry $registry,
            \Magento\Customer\Model\Session $customerSessionModel,
            \Magento\Framework\View\Result\PageFactory $resultRawFactory,
            \Knowband\Marketplace\Helper\Setting $settingHelper,
            \Knowband\Marketplace\Helper\Seller $sellerHelper,
            \Knowband\Marketplace\Model\Seller $sellerModel,
            \Knowband\Marketplace\Model\Product $productToSellerModel,
            \Magento\Framework\Event\Manager $eventManager,
            \Knowband\Marketplace\Helper\Log $logHelper
    ) {
        parent::__construct($context, $request, $response, $storeManager, $registry, $customerSessionModel, $resultRawFactory, $settingHelper, $sellerHelper);
        $this->mp_request = $request;
        $this->mp_storeManager = $storeManager;
        $this->mp_resultRawFactory = $resultRawFactory;
        $this->mp_settingHelper = $settingHelper;
        $this->mp_sellerHelper = $sellerHelper;
        $this->mp_sellerModel = $sellerModel;
        $this->mp_productToSellerModel = $productToSellerModel;
        $this->_eventManager = $eventManager;
        $this->_registry = $registry;
        $this->_objectManager = $context->getObjectManager();
        $this->mp_logHelper = $logHelper;
    }

    public function execute() {
//        $this->isLoggedIn();
//        if (!$this->getRequest()->getParam('id')) {
//            $this->redirectToDashboard();
//        }
        $postedData = $this->getRequest()->getParams();
         $this-> saveProductMappingAction($postedData['id'],$postedData['vendor']);
            echo "<pre>";
            print_r($postedData);
            echo "</pre>";
            exit;
          
           
        
        
    }
    
    private function saveProductMappingAction($product_id,$vendor) {
        $need_approval = \Knowband\Marketplace\Helper\GridAction::APPROVED;
        if ($this->mp_settingHelper->getSettingByKey($this->_customerInfo['entity_id'], 'product_approval')) {
            $need_approval = \Knowband\Marketplace\Helper\GridAction::WAITING_APPROVAL;
        }

        try {
            $sellerProductId = null;
            $product_to_seller_coll = $this->mp_productToSellerModel->getCollection()
                    ->addFieldToFilter("seller_id", ["eq" => $this->_customerInfo['entity_id']])
                    ->addFieldToFilter("product_id", ["eq" => $product_id]);
             if ($product_to_seller_coll->getSize()) {
                 foreach($product_to_seller_coll as $productSeller){
                     $sellerProductId = $productSeller->getSellerProductId();
                 }
             }
                $product_to_seller = $this->mp_productToSellerModel
                        ->setWebsiteId($this->mp_storeManager->getStore()->getWebsiteId());
                if($sellerProductId!= null){
                  $product_to_seller = $this->mp_productToSellerModel
                        ->setSellerProductId($sellerProductId);   
                }
                $product_to_seller->addData(
                            
                            ['seller_id' => $this->_customerInfo['entity_id'],
                            'product_id' => $product_id, 
                            'approved' => $need_approval,
                            'price'=>$vendor['price'],
                            'qty'=>$vendor['qty']]);
                $product_to_seller->save();
                $product_to_seller->unsetData();

                if ($need_approval == \Knowband\Marketplace\Helper\GridAction::WAITING_APPROVAL) {
                    $this->_objectManager->get("\Knowband\Marketplace\Helper\Product")->statusUpdateOnProductApproval($product_id, $need_approval, $this->_customerInfo['entity_id']);
//                    $this->_objectManager->get("\Knowband\Marketplace\Helper\Email")->sendProductApprovalRequestEmail($this->_customerInfo['entity_id'], $product_id);
                }

                $msg = __('Product has been saved successfully.');
               if ($need_approval == \Knowband\Marketplace\Helper\GridAction::WAITING_APPROVAL) {
                    $msg .= ' ' . __('Please wait for admin approval.');
                }
                $this->messageManager->addSuccess($msg);
            
        } catch (\Exception $e) {
            $message = 'Failed to save new product(#' . $product_id . ') mapping with seller(#' . $this->_customerInfo['entity_id'] . '). Error - ' . $e->getMessage();
            $this->mp_logHelper->createFileAndWriteLogData(
                    \Knowband\Marketplace\Helper\Log::INFOTYPEERROR, 'Controller Product\Edit::saveProductMappingAction()', $message
            );
        }
    }
}
